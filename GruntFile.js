(function () {
   'use strict';
}());

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      options: {
        separator: '\r\n\r\n'
      },
      dist: {
        /* import jquery first so bootstrap loads ok */
        /* then import recursively in the JS folder */
        src: ['app/assets/js/vendor/jquery.min.js', 'app/assets/js/**/*.js'],
        dest: 'app/assets/js/main.js'
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'app/assets/js/main.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },

    sass: {
     dist: {
      options: {
        style: "compressed",
        lineNumbers: false
      },
      files: {
        'app/assets/css/main.min.css': 'app/assets/scss/main.scss'
      }
     }
    },

    watch: {
      files: ['app/assets/scss/main.scss', 'app/assets/js/**/*.js'],
      tasks: ['concat', 'uglify', 'sass']
    }

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'watch']);
};